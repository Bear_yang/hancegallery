package com.hanceedu.hancegallery;

import java.lang.reflect.Field;

import android.os.Bundle;
import android.app.Activity;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hanceedu.hancegallery.ScrollerLayout.OnItemClickListener;

public class MainActivity extends Activity {
	ImageView showImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        LinearLayout galleryLayout = (LinearLayout) findViewById(R.id.gallery_layout);
		showImage = (ImageView) findViewById(R.id.imageview_show);

		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT,
				LinearLayout.LayoutParams.WRAP_CONTENT);
		
		int tempCount = (int) (Math.random() * 5) + 3;
		System.out.println("加载了" + tempCount + "个Item");

		//取得所有图片的resource Id数组
		int[] resouceIDs = initResouceIDArray(R.array.icon_ids, R.drawable.class);
		
		for (int i = 0; i < tempCount; i++) {
			ScrollerLayout expanItem = new ScrollerLayout(this, resouceIDs);
			expanItem.setOnItemClickListener(new ShowlayoutClickLinstener());
			galleryLayout.addView(expanItem, lp);
		}
    }

    /**
	 * 通过配置arrays.xml资源文件，获取特定想要的真正资源ID
	 * @param resID 配置的arrays数组ID
	 * @param what 真正资源的class
	 * @return 真正资源Id的数组
	 */
	private int[] initResouceIDArray(int resID, Class<?> what) {
		//取得所有科目图片的resource Id数组
		String[] resouceIdsStrings = getResources().getStringArray(resID);
		//为resouceIDs分配空间
		int[] res_ids = new int[resouceIdsStrings.length];
		//遍历可选择科目数组，获得相应的resource Id
		for (int i = 0; i < resouceIdsStrings.length; i++) {
			//根据resource Id 字符串找到实际的 resource Id 值
			res_ids[i] = getResourceId(resouceIdsStrings[i], what);
		}
		return res_ids;
	}

	/**
	 * 根据resource Id 字符串找到实际的 resource Id 值
	 */
	public int getResourceId(String subject, Class<?> what) {
		if (subject == null || subject.trim().equals("")) {
			return -1;
		}
		try {
			Field field = what.getDeclaredField(subject);
			return field.getInt(subject);
		} catch (Exception e) {
			Toast.makeText(MainActivity.this, "警告:相应图标缺失", Toast.LENGTH_SHORT)
					.show();
			return -1;
		}
	}
	
	class ShowlayoutClickLinstener implements OnItemClickListener {

		@Override
		public void onItemClick(ViewGroup view, int postion, int resId) {
			view.setFocusableInTouchMode(true);
			// 获取焦点
			view.requestFocus();
			System.out.println(view.toString() + "第" + postion + "个Button");
			showImage.setImageResource(resId);
		}

	}

    
}
