package com.hanceedu.hancegallery;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class ScrollerLayout extends LinearLayout {

	private Handler messageHandler;
	/** 展开消息 */
	private static final int MSG_STRETCHING = 1;
	/** 折叠消息 */
	private static final int MSG_FOLD = 2;
	/** 动画每次移动间隔时间(毫秒)，数字越大越慢 */
	private static final int ANIM_SPEED = 5;
	/** 动画每次移动间隔像素，数字越大越快</br><b>特别注意:</b></br>这个值,必须要能被Item.width整除，否则会出问题 */
	private static final int ANIM_SPEEDPX = 8;
	
	public ScrollerLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ScrollerLayout(Context context) {
		this(context, new int[]{R.drawable.ic_launcher});
	}

	public ScrollerLayout(Context context, int[] resIDs) {
		super(context);
		for (int i = 0; i < resIDs.length; i++) {
			View tempView = new View(context);
			tempView.setBackgroundResource(resIDs[i]);
			final int resID = resIDs[i];
			tempView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					onItemClickListener.onItemClick(ScrollerLayout.this,
							indexOfChild(v), resID);
				}
			});
			this.addView(tempView, 64, 64);
		}
		for (int i1 = 1; i1 < this.getChildCount(); i1++) {
			getChildAt(i1).setVisibility(View.GONE);
		}
		setBackgroundResource(R.drawable.container_bg_selector);
		messageHandler = new MessageHandler(Looper.myLooper());
	}

	@Override
	protected void onFocusChanged(boolean gainFocus, int direction,
			Rect previouslyFocusedRect) {
		setFocusable(true);

		if (gainFocus) {
			StretchLooperThread looperThread = new StretchLooperThread();
			looperThread.start();
		} else {
			HideLooperThread hidelooperThread = new HideLooperThread();
			hidelooperThread.start();
		}
		super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
	}

	class MessageHandler extends Handler {
		// FIXME 做个标记，这里是MessageHandler
		public MessageHandler(Looper looper) {
			super(looper);
		}

		@Override
		public void handleMessage(Message msg) {
			setLayoutParams(new LayoutParams(msg.arg1 + getPaddingLeft()
					+ getPaddingRight(), LayoutParams.WRAP_CONTENT));
			System.out.println("================\nmsg1:" + msg.arg1);
			switch (msg.what) {
			case MSG_STRETCHING:
				if (getChildAt(msg.arg2).getVisibility() != VISIBLE) {
					getChildAt(msg.arg2).setVisibility(VISIBLE);
				}
				System.out.println("msg2:" + msg.arg2 + "\nGetCount" + getChildCount());
				break;

			case MSG_FOLD:
				for (int i1 = 1; i1 < getChildCount(); i1++) {
					getChildAt(i1).setVisibility(View.GONE);
				}
				System.out.println("is fold!!!");
				break;
			}
		}
	}

	class StretchLooperThread extends Thread {

		public void run() {
			int itemWidth = getChildAt(0).getWidth();
			for (int i = itemWidth; i <= itemWidth * getChildCount(); i += ANIM_SPEEDPX / 2) {
				Message message = Message.obtain();
				message.what = MSG_STRETCHING;
				message.arg1 = i;
				messageHandler.sendMessage(message);
				if (message.arg1 / itemWidth < getChildCount()) {
					message.arg2 = message.arg1 / itemWidth;
				}

				synchronized (this) {
					try {
						wait(ANIM_SPEED);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	class HideLooperThread extends Thread {

		public void run() {
			int itemWidth = getChildAt(0).getWidth();
			boolean isFold = true;
			int realWidth = getWidth();
			while (isFold) {
				Message message = Message.obtain();
				if(realWidth > itemWidth + getPaddingLeft() + getPaddingRight()){
					realWidth -= ANIM_SPEEDPX;
					synchronized (this) {
						try {
							wait(ANIM_SPEED);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				} else {
					realWidth -= getPaddingLeft() + getPaddingRight();
					message.what = MSG_FOLD;
					isFold = false;
				}
				message.arg1 = realWidth;
				messageHandler.sendMessage(message);
			}
		}
	}

	private OnItemClickListener onItemClickListener;

	public interface OnItemClickListener {
		/**
		 * @param view 当前点击的ScrollerLayout
		 * @param postion 当前点击Item的位置
		 * @param resId 当前Item背景的资源Id
		 */
		public void onItemClick(ViewGroup view, int postion, int resId);
	}

	public void setOnItemClickListener(OnItemClickListener itemClickListener) {
		this.onItemClickListener = itemClickListener;
	}
}
